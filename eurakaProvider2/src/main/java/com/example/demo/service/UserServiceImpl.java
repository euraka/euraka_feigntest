package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.service.intf.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	private static Map<Integer,User> map;
	
	static {
		map = new HashMap<>();
		for (int i=1; i<6; i++) {
			map.put(i, new User(i,"test" +i , "pwd" + i,8083));
		}
	}
	@Override
	public User getById(Integer id) {
		return map.get(id);
	}

}