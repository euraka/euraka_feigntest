package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
@EnableFeignClients(basePackages={"com.example.demo"})
@ComponentScan("com.example.demo")
@EnableEurekaClient
@SpringBootApplication
public class EurakaConsumeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurakaConsumeApplication.class, args);
	}
}
