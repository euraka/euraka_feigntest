package com.example.demo.model;

import java.io.Serializable;

public class User implements Serializable{
   /**
	 * 
	 */
 private static final long serialVersionUID = 1L;
 private int id;
 public User() {
	super();
}

private String name;
 private String passward;
 private int port;


public int getPort() {
	return port;
}

public void setPort(int port) {
	this.port = port;
}

public User(int id, String name, String passward, int port) {
	super();
	this.id = id;
	this.name = name;
	this.passward = passward;
	this.port = port;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getPassward() {
	return passward;
}

public void setPassward(String passward) {
	this.passward = passward;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}
}
