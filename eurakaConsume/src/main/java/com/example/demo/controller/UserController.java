package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.service.UserFeignService;

@RestController
public class UserController {
	@Autowired
	private UserFeignService userFeignService;
	@RequestMapping("user/get/{id}")
	 public User get(@PathVariable("id") Integer id) throws Exception {
		 // 使用 Feign 封装的模板
		return this.userFeignService.get(id);
}
}